//
//  DetailViewController.h
//  TestTapptic
//
//  Created by LeNgocDuy on 1/28/15.
//  Copyright (c) 2015 Le Ngoc Duy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (strong, nonatomic) id detailItem;

@property (weak, nonatomic) IBOutlet UIImageView *imgViewItem;
@property (weak, nonatomic) IBOutlet UILabel *lblname;
@property (weak, nonatomic) IBOutlet UIButton *btnAction;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

- (IBAction)iba_play:(id)sender;
@end

