//
//  DetailViewController.m
//  TestTapptic
//
//  Created by LeNgocDuy on 1/28/15.
//  Copyright (c) 2015 Le Ngoc Duy. All rights reserved.
//

#import "DetailViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface DetailViewController ()
{
    BOOL isFadeIn;
}

@end

@implementation DetailViewController

#pragma mark - Managing the detail item

- (void) dealloc
{
    _detailItem = nil;
}

- (void)setDetailItem:(id)newDetailItem {
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
            
        // Update the view.
//        [self configureView];
    }
}

- (void)configureView {
    // Update the user interface for the detail item.
    if (![_detailItem isKindOfClass:[NSDictionary class]]) {
        return;
    }
    NSString *name = [_detailItem objectForKey:@"name"];
    CGSize size = [self sizeOfText:name withFont:self.lblname.font];
    CGRect lblFrame = self.lblname.frame;
    lblFrame.size.height = 100*size.height;
    [self.lblname setFrame:lblFrame];
    [self.lblname setNumberOfLines:100];
    CGSize contentSize = self.scrollView.contentSize;
    contentSize.height += lblFrame.size.height;
    [self.scrollView setContentSize:contentSize];
    [self.scrollView setBackgroundColor:[UIColor yellowColor]];

    NSMutableString *textEdit = [NSMutableString stringWithString:name];
    for (int i = 0; i < 998; i++) {
        [textEdit appendFormat:@"\n%@", name];
    }
    [textEdit appendString:name];
    [self.lblname setText:textEdit];
    NSString *linkImage = [_detailItem objectForKey:@"image"];
    if ([linkImage isKindOfClass:[NSString class]] && linkImage.length > 0) {
        [self.imgViewItem sd_setImageWithURL:[NSURL URLWithString:[_detailItem objectForKey:@"image"]]];
    }
}

- (CGSize)sizeOfText:(NSString *)text withFont:(UIFont*) font
{
    if ([text respondsToSelector:@selector(sizeWithFont:)]) {
        return [text sizeWithFont:font];
    }  else {
        return [text sizeWithAttributes:@{NSFontAttributeName : font}];
    }
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self configureView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)iba_play:(id)sender {
    if (isFadeIn) {
        [self createFadeOutEffect];
    } else {
        [self createFadeInEffect];
    }
}

- (void) createFadeInEffect
{
    CGRect frame = self.scrollView.frame;
    frame.size.height = frame.size.height/2;
    [UIView animateWithDuration:0.5 delay:0.1 options:UIViewAnimationOptionCurveEaseIn animations:^{
        [self.scrollView setFrame:frame];
        [self.scrollView setAlpha:0.5];
    } completion:^(BOOL finished) {
        isFadeIn = YES;
        [self.btnAction setTitle:@"Fade out" forState:UIControlStateNormal];
    }];}

- (void) createFadeOutEffect
{
    CGRect frame = self.scrollView.frame;
    frame.size.height = frame.size.height*2;
    [UIView animateWithDuration:0.5 delay:0.1 options:UIViewAnimationOptionCurveEaseOut animations:^{
        [self.scrollView setFrame:frame];
        [self.scrollView setAlpha:1.0f];
    } completion:^(BOOL finished) {
        isFadeIn = NO;
        [self.btnAction setTitle:@"Fade in" forState:UIControlStateNormal];
    }];
}

@end
