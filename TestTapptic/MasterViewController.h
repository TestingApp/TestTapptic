//
//  MasterViewController.h
//  TestTapptic
//
//  Created by LeNgocDuy on 1/28/15.
//  Copyright (c) 2015 Le Ngoc Duy. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;

@interface MasterViewController : UITableViewController

@property (strong, nonatomic) DetailViewController *detailViewController;

@end

