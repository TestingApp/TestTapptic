//
//  ItemCell.h
//  TappticTest
//
//  Created by test on 1/28/15.
//  Copyright (c) 2015 test. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItemCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgViewItem;
@property (weak, nonatomic) IBOutlet UILabel *lblName;

- (void) reloadDataWithName:(NSString *)name andImageLink:(NSString *)imgLink;

@end
