//
//  AppDelegate.h
//  TestTapptic
//
//  Created by LeNgocDuy on 1/28/15.
//  Copyright (c) 2015 Le Ngoc Duy. All rights reserved.
//

#import <UIKit/UIKit.h>

#define NETWORK_AVAILABLE @"NETWORK_AVAILABLE"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
+(BOOL) isNetWorkValiable;

@end

