//
//  ItemCell.m
//  TappticTest
//
//  Created by test on 1/28/15.
//  Copyright (c) 2015 test. All rights reserved.
//

#import "ItemCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation ItemCell

- (void)awakeFromNib {
    // Initialization code
}

- (void) changeBackGroundColor
{
    [self setBackgroundColor:[UIColor redColor]];
    [self performSelector:@selector(resetColorToSelected) withObject:self afterDelay:0.2];
}

- (void) resetColorToSelected
{
    [self setBackgroundColor:[UIColor blackColor]];
    [self.lblName setTextColor:[UIColor whiteColor]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    if (selected) {
        [self changeBackGroundColor];
    } else {
        [self setBackgroundColor:[UIColor whiteColor]];
        [self.lblName setTextColor:[UIColor blackColor]];
    }
}

- (void) reloadDataWithName:(NSString *)name andImageLink:(NSString *)imgLink
{
    [self.lblName setText:name];
    [self.imgViewItem sd_setImageWithURL:[NSURL URLWithString:imgLink]];
}

- (void)prepareForReuse
{
    [self setBackgroundColor:[UIColor whiteColor]];
    [self.lblName setTextColor:[UIColor blackColor]];
}


@end
