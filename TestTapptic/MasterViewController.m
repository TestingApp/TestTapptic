//
//  MasterViewController.m
//  TestTapptic
//
//  Created by LeNgocDuy on 1/28/15.
//  Copyright (c) 2015 Le Ngoc Duy. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"
#import "AppDelegate.h"
#import "ItemCell.h"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


@interface MasterViewController ()

@property (nonatomic, strong) NSMutableArray *objects;
@property (nonatomic,strong) UIRefreshControl *refreshControl;
@property (nonatomic, strong) NSIndexPath *lastIndexFocus;

@end

@implementation MasterViewController


- (void) dealloc
{
    [_objects removeAllObjects];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        self.clearsSelectionOnViewWillAppear = YES;
        self.preferredContentSize = CGSizeMake(320.0, 600.0);
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self setTitle:@"List Item WS"];
    self.detailViewController = (DetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor clearColor];
    self.refreshControl.tintColor = UIColorFromRGB(0xe43c22);
    [self.refreshControl addTarget:self action:@selector(handleActionRefresh) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNotificationNetWorkAvailable) name:NETWORK_AVAILABLE object:nil];
    [self requestGetDataFromWS];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)insertNewObject:(id)sender {
    if (!self.objects) {
        self.objects = [[NSMutableArray alloc] init];
    }
    [self.objects insertObject:[NSDate date] atIndex:0];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        id object = self.objects[indexPath.row];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            DetailViewController *controller = (DetailViewController *)[segue destinationViewController];
            [controller setDetailItem:object];
            controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
            controller.navigationItem.leftItemsSupplementBackButton = YES;
        } else {
            UINavigationController *navController = (UINavigationController*)[segue destinationViewController];
            DetailViewController *controller = (DetailViewController *)[navController topViewController];
            [controller setDetailItem:object];
            controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
            controller.navigationItem.leftItemsSupplementBackButton = YES;
        }
    }
}

#pragma mark - Handle request WS
- (void) handleActionRefresh
{
    if (![AppDelegate isNetWorkValiable]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"NetWork Invalid" message:@"No Network connect. Please check your network then retry by pull to refresh" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] ;
        [alertView show];
        [self.refreshControl endRefreshing];
        return;
    }
    [self requestGetDataFromWS];
}

- (void) handleNotificationNetWorkAvailable
{
    if (![self.objects isKindOfClass:[NSArray class]] || self.objects.count == 0) {
        [self requestGetDataFromWS];
    }
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.objects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ItemCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ItemCell class])];
    
    if (cell == nil) {
        cell = [[ItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:NSStringFromClass([ItemCell class])];
    }
    NSDictionary *dic = [_objects objectAtIndex:indexPath.row];
    if ([dic isKindOfClass:[NSDictionary class]]) {
        [cell reloadDataWithName:[dic objectForKey:@"name"] andImageLink:[dic objectForKey:@"image"]];
    }
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([_lastIndexFocus isEqual:indexPath]) {
        return;
    }
    if ([_lastIndexFocus isKindOfClass:[NSIndexPath class]]) {
        [tableView deselectRowAtIndexPath:_lastIndexFocus animated:YES];
    }
    self.lastIndexFocus = indexPath;
}

#pragma mark - request data from webservice
- (void) requestGetDataFromWS
{
    NSURLRequest *request = [NSURLRequest requestWithURL:
                             [NSURL URLWithString:@"http://dev.tapptic.com/test/json.php"]];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data,
                                               NSError *connectionError) {
                               if ([data isKindOfClass:[NSData class]]) {
                                   NSError *error;
                                   NSArray *temp = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                                   dispatch_async(dispatch_get_main_queue(), ^{
                                       if ([temp isKindOfClass:[NSArray class]]) {
                                           self.objects = [[NSMutableArray alloc] initWithArray:temp];
                                           [self.tableView reloadData];
                                       } else {
                                           [self showAlertNetwork];
                                       }
                                       [self.refreshControl endRefreshing];
                                   });
                               } else {
                                   [self showAlertNetwork];
                               }
                           }];
    
}

- (void)showAlertNetwork
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"NO DATA. PLEASE CHECK YOUR WS THEN PULL TO REFRESH TO RETRY" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] ;
    [alertView show];
}

@end
